# DefiLe-un-garcon

Un homme a deux enfants. L'un d'eux est un garçon.
Quelle est la probabilité que l'autre soit un garçon aussi ?

(On supposera que la propabilité d'avoir l'un ou l'autre est toujours égale à 1/2…)

La réponse naïve est __1/2__. Il est en effet juste de dire que sachant que le premier est un garçon, alors l'autre l'est aussi une fois sur deux. Sauf que la question mentionne juste un garçon, sans préciser s'il est premier ou deuxième.

L'autre réponse naïve est __1/4__. Il est tout aussi juste de dire qu'avoir un garçon après avoir eu un garçon, seuls un quart des couples l'expérimente. Cependant, la question n'aborde pas l'ordre.

La réponse est __1/3__ ! Si vous comprenez le langage Python, tout y est expliqué.

## Réponse par le programme

__note :__ Chaque exécution du programme donnera des probabilités fluctuant autour des valeurs théoriques indiquées entre parenthèses.

Un homme a deux enfants. L'un d'eux est un garçon.
Quelle est la probabilité que l'autre soit un garçon aussi ? (1/3)
0.3333957319356046

Quelle est la probabilité d'avoir deux garçons ? (1/4)
0.24934

Quelle est la probabilité d'avoir un garçon puis un garçon ? (1/4)
0.24934

Quelle est la probabilité d'avoir un garçon sachant que le premier est un garçon ? (1/2)
0.49734710974588103

## LICENSE

WTFPL : https://fr.wikipedia.org/wiki/WTFPL
