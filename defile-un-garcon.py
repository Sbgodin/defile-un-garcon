#!/usr/bin/python3

# Un homme a deux enfants. L'un d'eux est un garçon. Quelle est la probabilité que l'autre soit un garçon aussi ?

import random

nbEssais = 100000         # Nombre d'essais
nbIlyaUnGarçon = 0        # Fois où au moins un des deux a été un garçon
nbAutreAussiUnGarçon = 0  # Fois où il y eu deux garçon (l'un, puis l'autre) 
nbPremierEstGarçon = 0    # Fois où le premier a été un garçon
nbDeuxièmeAussiGarçon = 0 # Fois où le deuxième a été un garçon, comme le premier

for i in range(nbEssais):
    enfantUn   = random.choice(("garçon", "fille"))
    enfantDeux = random.choice(("garçon", "fille"))
    
    if enfantUn == "garçon" or enfantDeux == "garçon":      # L'un d'eux est un garçon
        nbIlyaUnGarçon += 1
        if enfantUn == "garçon" and enfantDeux == "garçon": # L'autre est un garçon
            nbAutreAussiUnGarçon += 1

    if enfantUn == "garçon":       # Premier enfant, garçon
        nbPremierEstGarçon += 1
        if enfantDeux == "garçon": # Deuxième enfant, garçon comme le premier
            nbDeuxièmeAussiGarçon += 1

print("Un homme a deux enfants. L'un d'eux est un garçon.")
print("Quelle est la probabilité que l'autre soit un garçon aussi ? (1/3)")
print(nbAutreAussiUnGarçon/nbIlyaUnGarçon)
print()
print("Quelle est la probabilité d'avoir deux garçons ? (1/4)")
print(nbAutreAussiUnGarçon/nbEssais)
print()
print("Quelle est la probabilité d'avoir un garçon puis un garçon ? (1/4)")
print(nbDeuxièmeAussiGarçon/nbEssais)
print()
print("Quelle est la probabilité d'avoir un garçon sachant que le premier est un garçon ? (1/2)")
print(nbDeuxièmeAussiGarçon/nbPremierEstGarçon)
